from comet_ml import Experiment

import os

import torch
from torch.nn.functional import cross_entropy
from torch.utils.data import DataLoader

from dataset import MiniImageNet, FewShotSampler
from models import ConvNet

import matplotlib.pyplot as plt
import numpy as np

import sys
from tqdm import tqdm

import argparse

''' 
#extra imports
import sys
app_folder = os.path.dirname(os.path.abspath(__file__))
sys.path.append(app_folder)
'''


import time
class Timer():

    def __init__(self):
        self.o = time.time()

    def measure(self, p=1):
        x = (time.time() - self.o) / p
        x = int(x)
        if x >= 3600:
            return '{:.1f}h'.format(x / 3600)
        if x >= 60:
            return '{}m'.format(round(x / 60))
        return '{}s'.format(x)
    
class TrainingLog():
    def __init__(self):
        self.logs = {}
        self.t = 0
        
    def record(self, name, val):
        if not (name in self.logs):
            self.logs[name] = []
        self.logs[name].append([self.t, val])
        
    def step(self, t=1):
        self.t +=t
        
def ensure_path(path):
    if os.path.exists(path):
        if input('{} exists, remove? ([y]/n)'.format(path)) != 'n':
            shutil.rmtree(path)
            os.makedirs(path)
    else:
        os.makedirs(path)

def count_acc(logits, label):
    pred = torch.argmax(logits, dim=1)
    return (pred == label).type(torch.cuda.FloatTensor).mean().item()


def euclidean_metric(a, b):
    n = a.shape[0]
    m = b.shape[0]
    a = a.unsqueeze(1).expand(n, m, -1)
    b = b.unsqueeze(0).expand(n, m, -1)
    logits = -((a - b)**2).sum(dim=2)
    return logits

dataset_path = '/workspace/codes/protonet/prototypical-network-pytorch/materials'

parser = argparse.ArgumentParser()
parser.add_argument('--max-epoch', type=int, default=200)
parser.add_argument('--save-interval', type=int, default=20)
parser.add_argument('--shot', type=int, default=5)
parser.add_argument('--query', type=int, default=15)
parser.add_argument('--train-way', type=int, default=30)
parser.add_argument('--test-way', type=int, default=5)
parser.add_argument('--save-path', default='./save/proto-5')
parser.add_argument('--gpu', default='0')
parser.add_argument('--n-batch', type=int, default=200)
parser.add_argument('--dataset', default=dataset_path)
parser.add_argument('--comet-api', default=None)
parser.add_argument('--comet-projectname', default='prototypical-network-miniImagenet')
parser.add_argument('--comet-workspace', default='rexxar-rg')
args = parser.parse_args()

print(args)

os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
multi_gpus = device==torch.device('cuda') and torch.cuda.device_count() > 1
print('GPU(s)',device, torch.cuda.device_count())


n_way = args.train_way
n_way_val = args.test_way
n_shot = args.shot
n_query = args.query
n_batch = args.n_batch
max_epochs = args.max_epoch
save_interval = args.save_interval
save_path = args.save_path

logging = True if (args.comet_api is not None) else False    
if logging:
    
    hyper_params = {
        "n_way": n_way,
        "n_shot": n_shot,
        "n_query": n_query,
        "n_batch": n_batch,
        "save_path": save_path,
        "max_epochs": max_epochs,
    }

    comet = Experiment(api_key=args.comet_api, 
                            project_name=args.comet_projectname, 
                            workspace=args.comet_workspace)
    comet.log_parameters(hyper_params)
    comet.set_name('20200912-120123')
    
trainset = MiniImageNet(path=dataset_path,split='train')
train_sampler = FewShotSampler(trainset.labels, n_way, n_shot+n_query, n_batch)
train_loader = DataLoader(dataset=trainset, batch_sampler=train_sampler,
                          num_workers=8, pin_memory=True)

valset = MiniImageNet(path=dataset_path,split='val')
val_sampler = FewShotSampler(valset.labels, n_way_val, n_shot+n_query, n_batch)
val_loader = DataLoader(dataset=valset, batch_sampler=val_sampler,
                          num_workers=8, pin_memory=True)


model = ConvNet()
if multi_gpus:
    model = torch.nn.DataParallel(model, device_ids = range(torch.cuda.device_count()))
model.to(device)

model_to_save = model if not multi_gpus else model.module
    
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)

ensure_path(save_path)

label = torch.arange(n_way).repeat(n_query)
label = label.type(torch.LongTensor).to(device)

val_label = torch.arange(n_way_val).repeat(n_query)
val_label = val_label.type(torch.LongTensor).to(device)

step=0
best_acc = 0
timer = Timer()
logs = TrainingLog()
for e in range(1,max_epochs+1):
    
    model.train()
    
    t = n_shot*n_way
    progress = tqdm(total=n_batch, file=sys.stdout)
    
    for i, (x,y) in enumerate(train_loader):
        x = x.to(device)

        x_shot, x_query = x[:t], x[t:]

        proto = model(x_shot)
        proto = proto.reshape(n_shot, n_way, -1).mean(dim=0)# num_proto = n_class (n_way)

        logits = euclidean_metric(model(x_query), proto)
        loss = cross_entropy(logits, label)
        acc = count_acc(logits, label)

        logs.record('loss',loss.item())
        logs.record('acc', acc)
        logs.step()

        with comet.train():
            step+=1
            comet.log_metric('loss', loss.item(), step, e)
            comet.log_metric('acc', acc, step, e)

        progress.set_description('epoch {}/{}, loss={:.4f} acc={:.4f}'
                                 .format(e, max_epochs, loss.item(), acc))

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        progress.update()
        progress.refresh()

        proto = None; logits = None; loss = None
    
    progress.close()
    lr_scheduler.step()
    
    '''------------------Evaluation---------------------'''
    model.eval()
    
    v_loss = []
    v_acc = []
    
    t = n_shot*n_way_val
    
    for i, (x,y) in enumerate(val_loader):
        x = x.to(device)

        x_shot, x_query = x[:t], x[t:]

        proto = model(x_shot)
        proto = proto.reshape(n_shot, n_way_val, -1).mean(dim=0)# num_proto = n_class (n_way)

        logits = euclidean_metric(model(x_query), proto)
        loss = cross_entropy(logits, val_label)
        acc = count_acc(logits, val_label)

        v_loss.append(loss.item())
        v_acc.append(acc)

    v_loss = np.mean(v_loss)
    v_acc = np.mean(v_acc)
    
    
    logs.record('val-loss',v_loss.item())
    logs.record('val-acc', v_acc)

    with comet.test():
        comet.log_metric('loss', v_loss.item(), step, e)
        comet.log_metric('acc', v_acc, step, e)
    
    if v_acc>best_acc: 
        torch.save(model_to_save.state_dict(), os.path.join(save_path, 'best' + '.pth'))
    if e%save_interval == 0 or e==max_epochs:
        torch.save(model_to_save.state_dict(), os.path.join(save_path, 'epoch-{}'.format(e) + '.pth'))
        torch.save(logs.logs, os.path.join(save_path, 'logs'))
    
    print('ETA:{}/{}'.format(timer.measure(), timer.measure(float(e) / max_epochs)),
          'epoch {}, val, loss={:.4f} acc={:.4f}'.format(e, v_loss, v_acc))
    
    