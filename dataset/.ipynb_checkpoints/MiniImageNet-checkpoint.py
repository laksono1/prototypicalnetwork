import os
from PIL import Image

from torch.utils.data import Dataset
from torchvision import transforms

class MiniImageNet(Dataset):

    def __init__(self, split, path):
        csv_path = os.path.join(path, split+'.csv')

        # ignore first line (header)
        lines = [x.strip() for x in open(csv_path, 'r').readlines()[1:]]

        filenames = []
        labels = []

        label = -1

        w_labels = {}
        for l in lines:
            name, wordnet = l.split(',')
            if not (wordnet in w_labels):
                label+=1
                w_labels[wordnet] = label

            fname = os.path.join(path, 'images', name)
            filenames.append(fname)
            labels.append(w_labels[wordnet])

        self.filenames = filenames
        self.labels = labels
        self.split = split

        self.preprocessor = {
            'train':transforms.Compose([
                transforms.Resize(90),
                transforms.RandomCrop(84),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
            ]),
            'test':transforms.Compose([
                transforms.Resize(84),
                transforms.CenterCrop(84),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
            ])
        }

    def preprocess(self, image):
        split = 'train' if self.split=='train' else 'test'
        transform = self.preprocessor[split]
        return transform(image)

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, i):
        fname, label = self.filenames[i], self.labels[i]
        image = self.preprocess(Image.open(fname).convert('RGB'))
        return image, label
