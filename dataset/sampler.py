import torch
import numpy as np

class FewShotSampler():
    
    def __init__(self, labels, n_cls, samples_per_class, n_batch):
        self.n_batch = n_batch
        self.n_cls = n_cls
        self.n_per = samples_per_class

        labels = np.array(labels)
        self.m_ind = []
        for i in range(max(labels) + 1):
            ind = np.argwhere(labels == i).reshape(-1)
            ind = torch.from_numpy(ind)
            self.m_ind.append(ind)
        
        self.n_labels = len(self.m_ind)
        
    def __len__(self):
        return self.n_batch
    
    def __iter__(self):
        for i_batch in range(self.n_batch):
            batch = []
            classes = torch.randperm(self.n_labels)[:self.n_cls]
            for c in classes:
                l = self.m_ind[c] # index of samples with class c
                pos = torch.randperm(len(l))[:self.n_per] # randomize and pick only n_per-first indices
                batch.append(l[pos]) # store the chosen sample indices
            
            batch = torch.stack(batch).t().reshape(-1) #original form is n_class*(supp+query), transpose
   
            yield batch