import torch
from torch.utils.data import DataLoader

from dataset import MiniImageNet, FewShotSampler
from models import ConvNet

import argparse
import os

def count_acc(logits, label):
    pred = torch.argmax(logits, dim=1)
    return (pred == label).type(torch.cuda.FloatTensor).mean().item()


def euclidean_metric(a, b):
    n = a.shape[0]
    m = b.shape[0]
    a = a.unsqueeze(1).expand(n, m, -1)
    b = b.unsqueeze(0).expand(n, m, -1)
    logits = -((a - b)**2).sum(dim=2)
    return logits

dataset_path = '/workspace/codes/protonet/prototypical-network-pytorch/materials'

parser = argparse.ArgumentParser()
parser.add_argument('--shot', type=int, default=5)
parser.add_argument('--query', type=int, default=30)
parser.add_argument('--way', type=int, default=5)
parser.add_argument('--checkpoint', default='./save/proto-5/best.pth')
parser.add_argument('--gpu', default='1')
parser.add_argument('--n-batch', type=int, default=2000)
parser.add_argument('--dataset', default=dataset_path)
parser.add_argument('--seed', type=int, default=None)
parser.add_argument('--split', default='test')
args = parser.parse_args()
print(args)

os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
multi_gpus = device==torch.device('cuda') and torch.cuda.device_count() > 1
print('GPU(s)',device, torch.cuda.device_count(),multi_gpus)

if args.seed is not None:
    torch.manual_seed(args.seed)
    
n_way = args.way
n_shot = args.shot
n_query = args.query
n_batch = args.n_batch
save_path = args.checkpoint
split = args.split

dataset = MiniImageNet(path=dataset_path,split=split)
sampler = FewShotSampler(dataset.labels, n_way, n_shot + n_query, n_batch)
loader = DataLoader(dataset, batch_sampler=sampler, num_workers=8, pin_memory=True)

model = ConvNet()
model.load_state_dict(torch.load(save_path))
if multi_gpus:
    model = torch.nn.DataParallel(model, device_ids = range(torch.cuda.device_count()))
model.to(device)

model.eval()

k = n_way * n_shot
label = torch.arange(n_way).repeat(n_query)
label = label.type(torch.cuda.LongTensor)

eval_acc = 0.
for i, (x,y) in enumerate(loader, 1):
    x = x.cuda()
    
    x_shot, x_query = x[:k], x[k:]

    proto = model(x_shot)
    proto = proto.reshape(n_shot, n_way, -1).mean(dim=0)

    logits = euclidean_metric(model(x_query), proto)

    acc = count_acc(logits, label)
    eval_acc += acc
    print('batch {}: {:.2f}({:.2f})'.format(i, eval_acc/(i+1) * 100, acc * 100))

    x = None; p = None; logits = None

